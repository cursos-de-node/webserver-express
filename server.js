const express = require('express');
const app = express();
const hbs = require('hbs');

//Importar archivo con helpers
require('./hbs/helpers');

//Asigna un puerto
const puerto = process.env.PORT || 3000;

//midelwware = Intruccion o callback que se ejecutara siempre sin importar la url
app.use(express.static(__dirname + '/public'));

//Express hbs engine
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');


app.get('/', function(req, res) {
    res.render('home', {
        nombre: 'miguel pacheco'
    });
});

app.get('/about', function(req, res) {
    res.render('about');
});



// app.get('/', function(req, res) {
//     //res.send('Hola mundo');
//     let salida = {
//         nombre: 'Miguel',
//         apellido: 'Pacheco'
//     };
//     res.send(salida);
// });

app.listen(puerto, () => {
    console.log(`Escuchando desde el puerto ${puerto}`);
});